# README

Example setup to explore the processing of Ordinance Survey open data in GML format.

Overview
![Overview Diagram](docs/overview.png "Overview")

Technology
* ogr2ogr to convert the GML data to postgis' format
* Postgres and PostGis as the data store
* Geoserver to expose that data through standard interfaces - in particular wms
* Open Layers to render an sample layer (the buildings layer in this case)


## How To

### Start up the stack
run with docker compose from the root directory
`docker-compose up`

### Shutdown the stack
`docker-compose down`

### Shutdown the stack and clear up data
`docker-compose down --volumes`

### Access Postgres Admin
`http://localhost:5050/browser/` - test@test.com:qwertyU1!
You need to add the postgis server - host=postgis user=geoserver pw=geoserver

### Access geoserver
`http://localhost:8080/geoserver` - admin:geoserver

### View the sample map
`http://localhost:8000/`

### Import data
Add your data to a "sample-data" folder in the root of the project.

>**NOTE**:
>The OS sample data (SX.gml) isn't included in the repo  due to size. It can be downloaded from here: https://www.ordnancesurvey.co.uk/business-government/products/mastermap-topography


Spin up the ogr2ogr container on the same network as the other services:
`docker run --network geoserver_default -it -v $(pwd)/sample-data:/data osgeo/gdal /bin/bash`

Run ogr to import the data, e.g. for the plymouth data:
`ogr2ogr -f PostgreSQL Pg:'dbname=geoserver host=postgis user=geoserver password=geoserver port=5432' data/SX.gml`

Or for the sample boxes:
`ogr2ogr -f PostgreSQL Pg:'dbname=geoserver host=postgis user=geoserver password=geoserver port=5432' data/sample-subset.gml`

## Limitations / TODOs

### geoserver_data folder

This folder is a mix of configuration, which needs to be commited to ensure the project runs when checked out, and operational data, which should not. I have only _excluded_ from version control what I am sure is not required for startup, but this is obviously not right yet and alot of operational data is still being included in commits.


## Notes

### Geoserver
* Needed to download and unzip latest data zip into the geoserver_data folder:
https://build.geo-solutions.it/geonode/geoserver/latest/
* Container: geonode/geoserver
* NOTE: sharing the docker.sock volume is dangerous - it allows the container to talk to the docker host - this is only for trying the server out on an isolated network with no modifications. Will need to look further into why this is required if I use it further

### PostGIS

For troubleshooting you can connect directly to the container to execute psql
`docker exec -ti {postgis container id} psql -U postgres`

### GDAL
* Repository: https://github.com/OSGeo/gdal/tree/master/docker
* Container: osgeo/gdal`
* We might need to explore more options as in the example below, but these aren't working at the moment:
`ogr2ogr -nln test_layer -nlt PROMOTE_TO_MULTI -lco GEOMETRY_NAME=geom -lco FID=gid -lco PRECISION=NO -f PostgreSQL Pg:'dbname=geoserver host=postgis user=geoserver password=geoserver port=5432' data/SX.gml`

### Setup Open Layers

Run a webserver
`python3 -m http.server`
