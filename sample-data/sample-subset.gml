<?xml version="1.0" encoding="UTF-8"?>
<os:FeatureCollection xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:gss="http://www.isotc211.org/2005/gss" xmlns:os="http://namespaces.os.uk/product/1.0" xmlns:gsr="http://www.isotc211.org/2005/gsr" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:oml="http://namespaces.os.uk/open/oml/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gts="http://www.isotc211.org/2005/gts" gml:id="OSOpenMapLocal" xsi:schemaLocation="http://namespaces.os.uk/open/oml/1.0 https://ordnancesurvey.co.uk/xml/open/oml/1.0/OSOpenMapLocal.xsd">
	<gml:boundedBy>
		<gml:Envelope srsName="urn:ogc:def:crs:EPSG::27700" srsDimension="2">
			<gml:lowerCorner>190000 30000</gml:lowerCorner>
			<gml:upperCorner>310000 124743.14</gml:upperCorner>
		</gml:Envelope>
	</gml:boundedBy>
	<os:metadata xlink:href="http://www.os.uk/xml/products/OML.xml"/>
	<os:featureMember>
		<oml:Building_v2 gml:id="id124EFFFF-A940-4A58-948B-B3428046396A">
			<oml:geometry>
				<gml:Surface gml:id="id124EFFFF-A940-4A58-948B-B3428046396A-0" srsName="urn:ogc:def:crs:EPSG::27700" srsDimension="2">
					<gml:patches>
						<gml:PolygonPatch>
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList>190000 30000 190000 124743.14 310000 124743.14 310000 30000 190000 30000</gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:PolygonPatch>
					</gml:patches>
				</gml:Surface>
			</oml:geometry>
			<oml:featureCode>15014</oml:featureCode>
			<valid_from>2022-07-05</valid_from>
			<valid_to>2022-07-07</valid_to>
		</oml:Building_v2>
	</os:featureMember>
	<os:featureMember>
		<oml:Building_v2 gml:id="id97B1BADC-172A-4831-9267-7A15E9552A7B">
			<oml:geometry>
				<gml:Surface gml:id="id97B1BADC-172A-4831-9267-7A15E9552A7B-0" srsName="urn:ogc:def:crs:EPSG::27700" srsDimension="2">
					<gml:patches>
						<gml:PolygonPatch>
							<gml:exterior>
								<gml:LinearRing>
									<gml:posList>200000 40000 300000 40000 300000 114743.14 200000 114743.14 200000 40000</gml:posList>
								</gml:LinearRing>
							</gml:exterior>
						</gml:PolygonPatch>
					</gml:patches>
				</gml:Surface>
			</oml:geometry>
			<oml:featureCode>15014</oml:featureCode>
			<valid_from>2022-07-07</valid_from>
			<valid_to>2022-07-09</valid_to>
		</oml:Building_v2>
	</os:featureMember>
</os:FeatureCollection>
