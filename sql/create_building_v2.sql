-- SEQUENCE: public.building_v2_ogc_fid_seq

-- DROP SEQUENCE IF EXISTS public.building_v2_ogc_fid_seq;

CREATE SEQUENCE IF NOT EXISTS public.building_v2_ogc_fid_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

ALTER SEQUENCE public.building_v2_ogc_fid_seq
    OWNER TO geoserver;

-- Table: public.building_v2

-- DROP TABLE IF EXISTS public.building_v2;

CREATE TABLE IF NOT EXISTS public.building_v2
(
    ogc_fid integer NOT NULL DEFAULT nextval('building_v2_ogc_fid_seq'::regclass),
    gml_id character varying COLLATE pg_catalog."default" NOT NULL,
    featurecode integer,
	valid_from date,
	valid_to date,
    geometry geometry(Polygon,27700),
    CONSTRAINT building_v2_pkey PRIMARY KEY (ogc_fid)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.building_v2
    OWNER to geoserver;
-- Index: building_v2_geometry_geom_idx

-- DROP INDEX IF EXISTS public.building_v2_geometry_geom_idx;

CREATE INDEX IF NOT EXISTS building_v2_geometry_geom_idx
    ON public.building_v2 USING gist
    (geometry)
    TABLESPACE pg_default;
	
ALTER SEQUENCE IF EXISTS public.building_v2_ogc_fid_seq
   OWNED BY building_v2.ogc_fid;